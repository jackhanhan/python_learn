#!/usr/bin/python
# Filename:using_list.py

shopList=['apple','mango','carrrot','banana']
print 'I have' ,len(shopList),'items to purchase.'

print 'These items are:',
for item in shopList:
    print item,

print '\nI also have to buy rise.'
shopList.append('rice')
print 'My shopping list is now',shopList

print 'I will sort my list now'
shopList.sort()
print 'Sorted shopping list is',shopList

print 'The first item i will buy is',shopList[0]
olditem=shopList[0]
del shopList[0]
print 'I bought the ', olditem
print 'My shopping list is now ',shopList
